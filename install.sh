#!/bin/bash

#wget -O - https://gitlab.com/JonW/AutoFlush-NanoPi/raw/master/install.sh | bash

DIRECTORY="/usr/local/bin/AutoFlush"
systemctl stop AutoFlush.service

if [ -d "$DIRECTORY" ]; then
	echo "Upgrade AutoFlush the latest version"
	git -C $DIRECTORY fetch --all
	git -C $DIRECTORY reset --hard origin/master

else
	echo "Install AutoFlush"
	echo "Install AutoFlush"
	apt-get --yes install git python3-pip python3-setuptools python3-wheel
	echo "Install gpio"
	pip3 install gpio
	echo "Install schedule"
	pip3 install schedule
	mkdir $DIRECTORY
	chmod 710 $DIRECTORY
	git -C $DIRECTORY clone https://gitlab.com/JonW/AutoFlush-NanoPi.git .
fi

cp $DIRECTORY/AutoFlush.service /lib/systemd/system/AutoFlush.service

systemctl daemon-reload
systemctl enable AutoFlush.service
systemctl start AutoFlush.service