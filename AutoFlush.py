import gpio         #pip3 install gpio #http://wiki.friendlyarm.com/wiki/index.php/NanoPi_NEO
import time         #import the time library
import signal
import schedule     #pip3 install schedule / https://github.com/dbader/schedule
import datetime

def term(signal,frame):
    print ("Ctrl+C captured, ending read.")
    continue_loop = False
    Flusher0.Stop()
    Flusher1.Stop()
    gpio.cleanup()
    print ("Goodbuy")

def PeekJob():
    print("Peek Job Start")
    Flusher0.Flush(PeekDuration)
    Flusher1.Flush(PeekDuration)
    print("Peek Job Finish")

def OffPeekJob():
    print("Off-Peek Job Start")
    Flusher0.Flush(OffPeekDuration)
    Flusher1.Flush(OffPeekDuration)
    print("Off-Peek Job Finish")

class Flusher(object):
    def __init__(self,pin):
        self.relay = pin
        try:
            gpio.setup(self.relay, 'out')
        except:
            print("Flusher Failure on pin "+str(self.relay))
            exit(10)
        self.Stop()
        print("Flusher Ready on pin "+str(self.relay))
        
    def __del__(self):
        class_name = self.__class__.__name__
        print (class_name, "finished")
        
    def Start(self):
        print("Open Valve on pin "+str(self.relay))
        gpio.set(self.relay, 0)  #Relay On

    def Stop(self):
        print("Close Valve on pin "+str(self.relay))
        gpio.set(self.relay, 1)  #Relay Off

    def Flush(self, duration):
        print("Start Flush on pin "+str(self.relay))
        self.Start()
        print("Flush for "+str(duration)+" seconds.")
        time.sleep(duration)        #sleep for the duration of the flush
        self.Stop()

if __name__ == "__main__":
    
    # Hook the SIGINT
    signal.signal(signal.SIGINT, term)
    signal.signal(signal.SIGTERM, term)

    #CONFIG
    Peek = None
    continue_loop = True

    #Flush Peek
    PeekDays = [0,1,2,3,4] #Monday = 0
    PeekStart = datetime.time(7,0)
    PeekEnd = datetime.time(16,30)

    #Rate of Flush
    PeekFlush = 1
    OffPeekFlsuh = 2
    SensorFlushCount = 25 #Number of sensor activations to start a flush

    #Flush Duration
    PeekDuration = 20
    OffPeekDuration = 10

    Flusher0 = Flusher(0)
    Flusher1 = Flusher(2)

    while continue_loop:
        CurrentDay = datetime.datetime.today().weekday()
        CurrentTime = datetime.datetime.now().time()
        #Detect Peek Period
        if((CurrentDay in PeekDays) and (PeekStart <= CurrentTime <= PeekEnd)):
            if(Peek is False or Peek is None):
                print("Set Peek flush schedule")
                if(Peek is False):
                    print("Remove Off-Peek Schedule")
                    schedule.cancel_job(OffPeekSchedule)
                PeekSchedule = schedule.every(PeekFlush).minutes.do(PeekJob)
                #PeekSchedule = schedule.every(PeekFlush).seconds.do(PeekJob)
            Peek = True
        else:
            if(Peek is True or Peek is None):
                print("Set Off-Peek flush schedule")
                if(Peek is True):
                    print("Remove Peek Schedule")
                    schedule.cancel_job(PeekSchedule)
                OffPeekSchedule = schedule.every(OffPeekFlsuh).minutes.do(OffPeekJob)
                #OffPeekSchedule = schedule.every(OffPeekFlsuh).seconds.do(OffPeekJob)
            Peek = False

        schedule.run_pending()
        #print('.', end="")
        time.sleep(1)